connection: "historical_data"

# include all the views
include: "/views/**/*.view"

datagroup: user_history_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

 persist_with: user_history_default_datagroup

explore: history {
  label: "Inital Explore"
  join: c2c_true_min_max_date {
    type: inner
    relationship: one_to_one
    sql_on: ${history.username} = ${c2c_true_min_max_date.history_username} ;;
  }
}

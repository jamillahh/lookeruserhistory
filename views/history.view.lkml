view: history {
  sql_table_name: looker.history ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: bill_start {
    type: string
    sql: ${TABLE}.billStart ;;
  }

  dimension: bill_type {
    type: string
    sql: ${TABLE}.billType ;;
  }

  dimension: c2c_enabled {
    type: yesno
    sql: ${TABLE}.c2c = 'TRUE';;
  }

  dimension: created_on {
    type: date_time
    sql: ${TABLE}.createdOn ;;
  }

  dimension_group: creation {
    type: time
    timeframes: [time, date, week, month, raw, quarter, year]
    sql: ${TABLE}.createdOn ;;
  }

  dimension: dynamic_intercom_enabled {
    type: string
    sql: ${TABLE}.dynamicIntercomEnabled ;;
  }

  dimension: firm_id {
    type: number
    sql: ${TABLE}.firmId ;;
  }

  dimension: firm_name {
    type: string
    sql: ${TABLE}.firmName ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.firstName ;;
  }

  dimension: gr_archive {
    type: string
    sql: ${TABLE}.grArchive ;;
  }

  dimension: journal {
    type: string
    sql: ${TABLE}.journal ;;
  }

  dimension_group: last_login {
    type: time
    timeframes: [time, date, week, month, raw, quarter, year]
    sql: ${TABLE}.lastlogin ;;
  }

  dimension: lastlogin_on {
    type: string
    sql: ${TABLE}.lastlogin ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.lastName ;;
  }

  dimension: products {
    type: string
    sql: ${TABLE}.products ;;
  }

  dimension: billingamount {
    type: number
    sql: CASE when ${products} = 'C9 Ambient' Then 100
              when ${products} = 'C9 Intercom' Then 200
              when ${products} = 'C9 Trader Professional' Then 400
              when ${products} = 'C9 Trader Enterprise' Then 1000
         END;;
  }

  dimension: recording_storage {
    type: string
    sql: ${TABLE}.recordingStorage ;;
  }

  dimension_group: report {
    type: time
    timeframes: [time, date, week, month, raw, quarter, year]
    sql: ${TABLE}.reportedDate ;;
  }

  dimension: reported_on {
    type: string
    sql: ${TABLE}.reportedDate ;;
  }

  dimension: role_name {
    type: string
    sql: ${TABLE}.roleName ;;
  }

  dimension: sales_person {
    type: string
    sql: ${TABLE}.salesPerson ;;
  }

  dimension: source_name {
    type: string
    sql: ${TABLE}.sourceName ;;
  }

  dimension: sso_username {
    type: string
    sql: ${TABLE}.ssoUsername ;;
  }

  dimension: transcription_activated {
    type: yesno
    sql: ${TABLE}.transcriptionEnabled = "TRUE" ;;
  }

  dimension: user_id {
    type: number
    sql: ${TABLE}.userId ;;
  }

  dimension: username {
    type: string
    sql: ${TABLE}.username ;;
  }

  dimension: usr_city {
    type: string
    sql: ${TABLE}.usrCity ;;
  }

  dimension: usr_country {
    type: string
    sql: ${TABLE}.usrCountry ;;
  }

  dimension: usr_county {
    type: string
    sql: ${TABLE}.usrCounty ;;
  }

  dimension: usr_state {
    type: string
    sql: ${TABLE}.usrState ;;
  }

  dimension: usr_street1 {
    type: string
    sql: ${TABLE}.usrStreet1 ;;
  }

  dimension: usr_street2 {
    type: string
    sql: ${TABLE}.usrStreet2 ;;
  }

  dimension: usr_zip {
    type: string
    sql: ${TABLE}.usrZip ;;
  }

  dimension: voice_recording {
    type: yesno
    sql: ${TABLE}.voiceRecording = "TRUE" ;;
  }

  dimension: full_name {
    type: string
    sql: CONCAT(${first_name},' ',${last_name}) ;;
  }

  dimension: days_since_signup {
    type: number
    sql: datediff(now(), ${creation_date}) ;;
  }

  dimension: days_since_last_login {
    type: number
    sql: datediff(now(), ${last_login_date}) ;;
  }

  dimension: days_since_signup_tier {
    type: tier
    tiers: [0, 30, 90, 180, 360, 720]
    sql: ${days_since_signup} ;;
    style:  integer
  }

  dimension: days_since_last_login_tier {
    type: tier
    tiers: [0, 30, 90, 180, 360, 720]
    sql: ${days_since_last_login} ;;
    style:  integer
  }

  dimension: days_bewtween_signup_and_last_login {
    type: number
    sql: datediff(${last_login_date}, ${creation_date}) ;;
  }

  dimension: is_new_user {
    type: yesno
    sql: ${days_since_signup} <= 30 ;;
  }

#  dimension: is_new_user {
#    type: yesno
#    sql: ${days_since_signup} <= 90 ;;
#  }

  dimension: active_7_day {
    type: yesno
    sql: ${days_since_last_login} < 7 ;;
  }

  measure: number_of_unique_users {
    type: count_distinct
    sql: ${username} ;;
  }

  measure: number_of_unique_users_c2c {
    type: count_distinct
    sql: ${username} ;;
    filters: [c2c_enabled: "yes"]
  }

  measure: number_of_unique_users_transcription {
    type: count_distinct
    sql: ${username} ;;
    filters: [transcription_activated: "yes"]
  }

  measure: number_of_unique_users_voice_recording {
    type: count_distinct
    sql: ${username} ;;
    filters: [voice_recording: "yes"]
  }



  measure: user_count_active_7_days{
    type: count_distinct
    sql: ${username}
    ;;
    filters: [active_7_day: "yes"]
  }

  measure: max_report_date {
    type: date
    sql:  max(${report_date}) ;;
  }

  measure: min_report_date {
    type: date
    sql:  min(${report_date}) ;;
  }

  measure: max_login_date {
    type: date
    sql:  max(${last_login_date}) ;;
  }

  measure: min_login_date {
    type: date
    sql:  min(${last_login_date}) ;;
  }



#  measure: trans_activated {
#    type: yesno
#    sql: ${history.transcription_activated} != offset(${history.transcription_activated},-1);;
#  }
#
#

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: c2c_count {
    type: count
    filters: [c2c_enabled: "Yes"]
    drill_fields: [detail*]
  }

  measure: recording_count {
    type: count
    filters: [voice_recording: "Yes"]
    drill_fields: [detail*]
  }

  measure: transcription_count {
    type: count
    filters: [transcription_activated: "Yes"]
    drill_fields: [detail*]
  }

  measure: users_count {
    type: count_distinct
    sql:  ${firm_id} ;;
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      role_name,
      c2c_enabled,
      username,
      sso_username,
      first_name,
      last_name,
      firm_name
    ]
  }
}

view: enabled_dates {
  derived_table: {
    sql:
      SELECT
        username,
        MIN(report_date) AS c2c_enabled_date
      FROM
        history
      WHERE
        c2c_enabled = 'TRUE'
      GROUP BY
        username ;;
  }
}

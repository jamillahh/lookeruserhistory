view: c2c_true_min_max_date {
  derived_table: {
    sql: SELECT
        history.username  AS `history.username`,
        DATE(min((DATE(history.reportedDate ))) ) AS `history.min_report_date`,
        DATE(max((DATE(history.reportedDate ))) ) AS `history.max_report_date`
      FROM looker.history  AS history

      WHERE
        history.c2c = 'TRUE'
      GROUP BY 1
      ORDER BY DATE(min((DATE(history.reportedDate ))) ) DESC
      LIMIT 500
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: history_username {
    type: string
    sql: ${TABLE}.`history.username` ;;
  }

  dimension: history_min_report_date {
    type: date
    sql: ${TABLE}.`history.min_report_date` ;;
  }

  dimension: history_max_report_date {
    type: date
    sql: ${TABLE}.`history.max_report_date` ;;
  }

  set: detail {
    fields: [history_username, history_min_report_date, history_max_report_date]
  }
}
